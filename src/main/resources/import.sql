INSERT INTO user(id, username) VALUES (1, 'hector');
INSERT INTO user(id, username) VALUES (2, 'pedro');
INSERT INTO user(id, username) VALUES (3, 'luis');

INSERT INTO item(id, description, title) VALUES (1, 'item 1', 'title item 1');
INSERT INTO item(id, description, title) VALUES (2, 'item 2', 'title item 2');
INSERT INTO item(id, description, title) VALUES (3, 'item 3', 'title item 3');

INSERT INTO review(id, comment, rating, author_id, item_id) VALUES (1, 'comment 1', 10.0, 1,1);
INSERT INTO review(id, comment, rating, author_id, item_id) VALUES (2, 'comment 2', 8.0, 1,2);
INSERT INTO review(id, comment, rating, author_id, item_id) VALUES (3, 'comment 3', 5.0, 2,3);