package com.devskiller.service;

import com.devskiller.dao.ItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

    private final ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<String> getTitlesWithAverageRatingLowerThan(Double rating) {
        //TODO
        List <String> titles = itemRepository.findItemsWithAverageRatingLowerThan(rating).stream().map(item -> {
            return item.getReviews() == null ? "0" : item.getTitle();
        }).collect(Collectors.toList());

        return titles;
    }

}
