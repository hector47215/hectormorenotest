package com.devskiller.dao;

import com.devskiller.model.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Long> {

    //TODO
    @Query(value = "select * from item i inner join review r on r.item_id=i.id where r.rating < :rating", nativeQuery = true)
    List<Item> findItemsWithAverageRatingLowerThan(Double rating);
}
